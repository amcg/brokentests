﻿using System;
using System.IO;
using NUnit.Framework;

namespace BrokenTests
{
    public class Logger
    {
        private string filePath;

        public Logger()
        {
            //Get the directory where the test .dll lives
            string workingDirectory = TestContext.CurrentContext.TestDirectory;

            //Create a Path specifically made to store log files
            string exactPath = Path.Combine(workingDirectory, "Logging");

            //Check if the path exists, create it if it doesn't
            if (!Directory.Exists(exactPath))
                Directory.CreateDirectory(exactPath);

            //Store the name of the log file being written to for other methods in the class to use
            filePath = Path.Combine(exactPath, $"{DateTime.Now.ToString("dd.MM.yy")}_Log.txt");
            
        }

        public void Write(string TextToLog)
        {
            using (StreamWriter sw = File.AppendText(filePath))
            {
                sw.WriteLine(DateTime.Now.ToString("HH:mm:ss")+ "\t" + TextToLog);
            }
        }
    }
}
